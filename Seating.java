package assignment6;

import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.atomic.AtomicBoolean;

import assignment6.Seat.Section;

/*
 * currently only holds configuration for BATES recital hall
 * holds seating configuration for theater
 * @author Robert
 */
public class Seating {
	
	
	//house middle - 22*14 = 308; house right - 24*7 + 2 = 170; house left - 22*7 + 7 = 161
	//private static final int NUM_SEATS_BATES = 639;
	private static final int NUM_SECTIONS_BATES = 3;
	private static final int BATES_MIDDLE_ROWS = 22;
	private static final int BATES_MIDDLE_ROW_SEATS = 14;
	private static final int BATES_RIGHT_ROWS = 24;
	private static final int BATES_RIGHT_ROW_SEATS = 7;
	private static final int BATES_LEFT_ROWS = 22;
	private static final int BATES_LEFT_ROW_SEATS = 7;
	
	private ConcurrentLinkedQueue<Seat> middle = new ConcurrentLinkedQueue<Seat>();
	private ConcurrentLinkedQueue<Seat> right = new ConcurrentLinkedQueue<Seat>();
	private ConcurrentLinkedQueue<Seat> left = new ConcurrentLinkedQueue<Seat>();
	
	
	private ConcurrentLinkedQueue<Seat>  current = middle;
	private ConcurrentLinkedQueue<Seat> next = right;//arbitrary
	private AtomicBoolean notified;
	
	
	public Seating()
	{
		notified = new AtomicBoolean(false);
		for(int i = 0; i < BATES_MIDDLE_ROWS; i++)
		{
			char row = (char)(i + 'A');
			if(row >= 'I')
			{
				row++;
				if(row >= 'O')
				{
					row++;
				}
			}
			
			String sRow = row + "";
		
			for(int j = 0; j < BATES_MIDDLE_ROW_SEATS; j++)
			{
				
				Seat temp = new Seat(Section.HOUSE_MIDDLE, sRow, j + 108);//starting seat number; take care of it later
				middle.add(temp);
			}
			
		}
		
		for(int i = 0; i < BATES_LEFT_ROWS; i++)
		{
			char row = (char)(i + 'C') ;
			if(row >= 'I')
			{
				row++;
				if(row >= 'O')
				{
					row++;
				}
			}
			String sRow = row + "";
			int numSeats = BATES_LEFT_ROW_SEATS;
			if(row == 'C')
			{
				numSeats--; //only 101-106 in row C
			}
			for(int j = 0; j < numSeats; j++)
			{
				
				Seat temp = new Seat(Section.HOUSE_LEFT, sRow, j + 101);
				left.add(temp);
			}
			
		}
		//AA 101-104 and 116-118
		String backRow = "AA";
		for(int i = 0; i < 4; i++)
		{
			left.add(new Seat(Section.HOUSE_LEFT, backRow, i+101));
		}
		
		for(int i = 0; i < 3; i++)
		{
			left.add(new Seat(Section.HOUSE_LEFT, backRow, i + 116));
		}
		
		for(int i = 0; i < BATES_RIGHT_ROWS; i++)
		{
			char row = (char)(i + 'A');
			if(row >= 'I')
			{
				row++;
				if(row >= 'O')
				{
					row++;
				}
			}
			
			String sRow = row + "";
		
			for(int j = 0; j < BATES_RIGHT_ROW_SEATS; j++)
			{
				
				Seat temp = new Seat(Section.HOUSE_RIGHT, sRow, j + 122);
				right.add(temp);
			}
			
		}
		//AA 127-128
		right.add(new Seat(Section.HOUSE_RIGHT, backRow, 127));
		right.add(new Seat(Section.HOUSE_RIGHT, backRow, 128));
	}
	
	/*
	 * @return int number of seats left
	 */
	public int getNumSeatsLeft()
	{
		return middle.size() + left.size() + right.size();
	}
	
	/*
	 * @return boolean true: seat config has notified sold out; false: seat config hasn't notified
	 */
	public boolean getNotified()
	{
		return notified.get();
	}
	
	/*
	 * notification has been received
	 */
	public void setNotified()
	{
		notified.set(true);
	}
	
	/*
	 * chooses next available seat
	 * @return Seat 
	 */
	public Seat bestAvailableSeat()
	{
		ConcurrentLinkedQueue<Seat> section = nextSection();
		if(section == null)
		{
			return null;
		}
		else
		{
			return section.poll();//check if available?
		}
	}
	
	/*
	 * chooses next section to choose seat from; rotation middle->left->middle->right->middle...
	 * @return ConcurrentLinkedQueue<Seat> next section
	 */
	private ConcurrentLinkedQueue<Seat> nextSection()
	{
		int count = 0;
		do
		{
			if(count > NUM_SECTIONS_BATES)
			{
				return null;//all sections are full
			}
			count++;
			if(current == middle)//chooses next section to pick from
			{
				current = next;
			}
			else
			{
				current = middle;
				next = next == right ? left : right;
			}
			
		}while(current.peek() == null);
		return current;
	}
}
