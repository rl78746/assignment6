package assignment6;

import java.util.Random;
import java.util.concurrent.ConcurrentLinkedQueue;

//import assignment6.StopWatch;

/*
 * Clients represents people buying tickets; the line is never empty
 * @author Robert
 */
public class Clients {
	private final long SEED = System.nanoTime();
	private Random rand = new Random(SEED);
	private ConcurrentLinkedQueue<Integer> clients = new ConcurrentLinkedQueue<Integer>();
	private int nextClientID = 1;//numbered starting at 1
	private final int LOWER_BOUND = 100;
	private final int UPPER_BOUND = 1000;
	
	/*
	 * create Clients list with initial random capacity 100 to 1000
	 */
	public Clients()
	{
		int initCapacity = rand.nextInt(UPPER_BOUND - LOWER_BOUND) + LOWER_BOUND;//fix magic numbers later
	
		for(int i = 1; i < initCapacity; i++)
		{
			clients.add(i);//numbered 1 to initCapacity
		}
		nextClientID += initCapacity;
		
	}
	
	/*
	 * return next client in line identified by number and add another client to ensure queue is never empty
	 */
	public int nextClient()
	{
		clients.add(nextClientID++);
		return clients.poll();
	}

	/*
	 * should never be true
	 */
	public boolean isEmpty()
	{
		return clients.isEmpty();
	}
}
