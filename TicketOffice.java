package assignment6;

/*
 * ticket office to sell seats to clients
 * @author Robert
 */
public class TicketOffice implements Runnable{
	private Clients myClients;
	
	private Seating seatConfig;
	private char identifier;
	
	/*
	 * Creates ticket office for myClients, selling tickets for theater, and has an identifier
	 * @param myClients people waiting in line
	 * @param theater seating configuration
	 * @param identifier which ticket office
	 */
	public TicketOffice(Clients myClients, Seating theater, char identifier)
	{
		this.myClients = myClients;
		seatConfig = theater;
		this.identifier = identifier;
	}
	
	/*
	 *sells seats to clients until there are no more seats 
	 *prints ticket information
	 */
	@Override
	public void run()
	{
		//System.out.println(seatConfig.getNumSeats());
		Seat nextSeat = seatConfig.bestAvailableSeat();
		
		while(nextSeat != null && !myClients.isEmpty())
		{
			printTicket(nextSeat);
			nextSeat = seatConfig.bestAvailableSeat();
			myClients.nextClient();
		}		
		if(!seatConfig.getNotified())
		{
			seatConfig.setNotified();
			System.out.println("Sorry, we are sold out.");
		}
		
	}
	
	/*
	 * formats ticket information
	 * @param aSeat which seat has been reserved
	 */
	private void printTicket(Seat aSeat)
	{
		System.out.println("Box Office " + this.identifier + ": Reserved " + aSeat.getSectionString() + ", " + aSeat.getSeatNumber() + aSeat.getRow());
	}
	
}
