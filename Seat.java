package assignment6;

/*
 * holds information about individual seats
 * @author Robert
 */
public class Seat {
	
	public enum Section{
		HOUSE_MIDDLE("HM"),
		HOUSE_RIGHT("HR"),
		HOUSE_LEFT("HL");
		
		private String abbr;
		private Section(String abbr)
		{
			this.abbr = abbr;
		}
		
		public String toString()
		{
			return abbr;
		}
	}
	
	private Section sec;
	private String row;
	private int seatNumber;
	
	/*
	 * creates seat with section, row, seat number
	 * @param sec which section seat is in
	 * @param row which row
	 * @param seatNumber seat number
	 */
	public Seat(Section sec, String row, int seatNumber)
	{
		this.sec = sec;
		this.row = row;
		this.seatNumber = seatNumber;
	}
	
	/*
	 * for printing
	 * @return String abbreviation of section
	 */
	public String getSectionString()
	{
		return sec.toString();
	}
	
	/*
	 * get row letter
	 * @return String row letter(s)
	 */
	public String getRow()
	{
		return row;
	}
	
	/*
	 * get seat number
	 * @return int seatNumber
	 */
	public int getSeatNumber()
	{
		return seatNumber;
	}

}
