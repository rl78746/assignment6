package assignment6;

public class A6Driver {

	/*
	 * start ticket office threads
	 * @author Robert
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Seating bates = new Seating();
		Clients stream = new Clients();
		
		TicketOffice office1 = new TicketOffice(stream, bates, 'A');
		TicketOffice office2 = new TicketOffice(stream, bates, 'B');
		TicketOffice office3 = new TicketOffice(stream, bates, 'C');
		
		Thread t1 = new Thread(office1);
		Thread t2 = new Thread(office2);
		Thread t3 = new Thread(office3);
		
		t1.start();
		t2.start();
		t3.start();
	}

}
